var convidados = [
    {
        nome:'Matheus',
        idade:23,
        bebeCerveja: false,
        bebeRefrigerante: true
    },

    {
        nome:'Ingrid',
        idade:22,
        bebeCerveja: true,
        bebeRefrigerante: false
    },

    {
        nome:'Glaudson',
        idade:16,
        bebeCerveja: true,
        bebeRefrigerante: true
    },

    {
        nome:'Janine',
        idade: 36,
        bebeCerveja: false,
        bebeRefrigerante: false
    }
]

for (let index = 0; index < convidados.length; index++) {
    if(convidados[index].bebeRefrigerante == true){
        alert("O(A) convidado(a) " + convidados[index].nome + " prefere refrigerante!")
    }else if(convidados[index].bebeCerveja == true && convidados[index].idade >= 18){
        alert("O(A) convidado(a) " + convidados[index].nome + " prefere cerveja!")
    }else{
        alert("O(A) convidado(a) " + convidados[index].nome +  " deverá beber suco!")
    }
    
}