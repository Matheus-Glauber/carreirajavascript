var input = document.getElementById("quantidade");
var botaoIncrementa = document.querySelector("#btn-incrementa");
var botaoDecrementa = document.querySelector("#btn-decrementa");

botaoIncrementa.addEventListener('click', incrementa);
botaoDecrementa.addEventListener('click', decrementa);

function incrementa(){
    input.value++;

    var item = botaoIncrementa.closest('.item');

    var precoItem = adicionaValorItem(item);

    adicionaValorTotal(precoItem);
}

function decrementa(){
    input.value--;

    var item = botaoDecrementa.closest('.item');

    var precoItem = retiraValorItem(item);

    retiraValorTotal(precoItem);
}

function adicionaValorItem(item){
    var elementoPreco = item.querySelector('.preco-item');
    return Number(elementoPreco.textContent);
}

function adicionaValorTotal(valor){
    var elementoTotal = document.querySelector('#total');
    elementoTotal.textContent = valor + Number(elementoTotal.textContent);
}

function retiraValorItem(item){
    var elementoPreco = item.querySelector('.preco-item');
    return Number(elementoPreco.textContent);
}

function retiraValorTotal(valor){
    var elementoTotal = document.querySelector('#total');
    elementoTotal.textContent = valor - Number(elementoTotal.textContent);
}

